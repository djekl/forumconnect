﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ForumConnect_SALT_Gen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Text = "ForumConnect SALT Generator";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = GenPassword();
            MessageBox.Show("Your salt has been generated.\nPlease copy this into your app and the PHP code.", "Salt Created");
        }

        /// <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <param name="lowerCase">If true, generate lowercase string</param>
        /// <returns>Random string</returns>
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        private string GenPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(64, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(128, false));
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(builder.ToString()));
        }
    }
}
