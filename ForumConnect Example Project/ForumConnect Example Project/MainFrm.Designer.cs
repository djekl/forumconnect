﻿namespace ForumConnect_Example_Project
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.TestBox = new System.Windows.Forms.PictureBox();
            this.LoginBtn = new System.Windows.Forms.Button();
            this.FC_Info = new System.Windows.Forms.GroupBox();
            this.SALT = new System.Windows.Forms.TextBox();
            this.FC_URL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginCreds = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PasswordTxt = new System.Windows.Forms.TextBox();
            this.UsernameTxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TestBox)).BeginInit();
            this.FC_Info.SuspendLayout();
            this.LoginCreds.SuspendLayout();
            this.SuspendLayout();
            // 
            // TestBox
            // 
            this.TestBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TestBox.BackColor = System.Drawing.Color.Transparent;
            this.TestBox.Location = new System.Drawing.Point(0, 124);
            this.TestBox.Name = "TestBox";
            this.TestBox.Size = new System.Drawing.Size(30, 30);
            this.TestBox.TabIndex = 0;
            this.TestBox.TabStop = false;
            this.TestBox.DoubleClick += new System.EventHandler(this.TestBox_DoubleClick);
            // 
            // LoginBtn
            // 
            this.LoginBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginBtn.BackColor = System.Drawing.Color.Silver;
            this.LoginBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.LoginBtn.Location = new System.Drawing.Point(404, 118);
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.Size = new System.Drawing.Size(75, 23);
            this.LoginBtn.TabIndex = 1;
            this.LoginBtn.Text = "Login";
            this.LoginBtn.UseVisualStyleBackColor = false;
            this.LoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // FC_Info
            // 
            this.FC_Info.BackColor = System.Drawing.Color.Transparent;
            this.FC_Info.Controls.Add(this.SALT);
            this.FC_Info.Controls.Add(this.FC_URL);
            this.FC_Info.Controls.Add(this.label2);
            this.FC_Info.Controls.Add(this.label1);
            this.FC_Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FC_Info.ForeColor = System.Drawing.Color.Black;
            this.FC_Info.Location = new System.Drawing.Point(12, 12);
            this.FC_Info.Name = "FC_Info";
            this.FC_Info.Size = new System.Drawing.Size(229, 106);
            this.FC_Info.TabIndex = 2;
            this.FC_Info.TabStop = false;
            this.FC_Info.Text = "ForumConnect  Info";
            this.FC_Info.DoubleClick += new System.EventHandler(this.FC_Info_DoubleClick);
            // 
            // SALT
            // 
            this.SALT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SALT.Location = new System.Drawing.Point(6, 81);
            this.SALT.Name = "SALT";
            this.SALT.Size = new System.Drawing.Size(217, 20);
            this.SALT.TabIndex = 1;
            // 
            // FC_URL
            // 
            this.FC_URL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FC_URL.Location = new System.Drawing.Point(6, 35);
            this.FC_URL.Name = "FC_URL";
            this.FC_URL.Size = new System.Drawing.Size(217, 20);
            this.FC_URL.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "SALT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ForumConnect URL";
            // 
            // LoginCreds
            // 
            this.LoginCreds.BackColor = System.Drawing.Color.Transparent;
            this.LoginCreds.Controls.Add(this.label4);
            this.LoginCreds.Controls.Add(this.label3);
            this.LoginCreds.Controls.Add(this.PasswordTxt);
            this.LoginCreds.Controls.Add(this.UsernameTxt);
            this.LoginCreds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LoginCreds.Location = new System.Drawing.Point(266, 12);
            this.LoginCreds.Name = "LoginCreds";
            this.LoginCreds.Size = new System.Drawing.Size(213, 88);
            this.LoginCreds.TabIndex = 3;
            this.LoginCreds.TabStop = false;
            this.LoginCreds.Text = "Login Credentials";
            this.LoginCreds.DoubleClick += new System.EventHandler(this.LoginCreds_DoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "PASS:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "USER:";
            // 
            // PasswordTxt
            // 
            this.PasswordTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordTxt.Location = new System.Drawing.Point(73, 57);
            this.PasswordTxt.Name = "PasswordTxt";
            this.PasswordTxt.Size = new System.Drawing.Size(132, 20);
            this.PasswordTxt.TabIndex = 1;
            // 
            // UsernameTxt
            // 
            this.UsernameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameTxt.Location = new System.Drawing.Point(73, 31);
            this.UsernameTxt.Name = "UsernameTxt";
            this.UsernameTxt.Size = new System.Drawing.Size(132, 20);
            this.UsernameTxt.TabIndex = 0;
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ForumConnect_Example_Project.Properties.Resources.sigs;
            this.ClientSize = new System.Drawing.Size(491, 154);
            this.Controls.Add(this.LoginCreds);
            this.Controls.Add(this.FC_Info);
            this.Controls.Add(this.LoginBtn);
            this.Controls.Add(this.TestBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ForumConnect Testing Application";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TestBox)).EndInit();
            this.FC_Info.ResumeLayout(false);
            this.FC_Info.PerformLayout();
            this.LoginCreds.ResumeLayout(false);
            this.LoginCreds.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox TestBox;
        private System.Windows.Forms.Button LoginBtn;
        private System.Windows.Forms.GroupBox FC_Info;
        private System.Windows.Forms.GroupBox LoginCreds;
        private System.Windows.Forms.TextBox SALT;
        private System.Windows.Forms.TextBox FC_URL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PasswordTxt;
        private System.Windows.Forms.TextBox UsernameTxt;
    }
}

