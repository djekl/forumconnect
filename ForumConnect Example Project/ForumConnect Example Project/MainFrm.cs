﻿using System;
using System.Windows.Forms;
using ForumConnect;

namespace ForumConnect_Example_Project
{
    public partial class MainFrm : Form
    {
        public MainFrm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = String.Format("ForumConnect v{0} Testing Application                     djekl Developments 2010~2013", System.Reflection.Assembly.LoadWithPartialName("ForumConnect").GetName().Version.ToString()); //System.Reflection.Assembly.GetExecutingAssembly().GetName().Version
            PasswordTxt.PasswordChar = '●';
        }

        private void TestBox_DoubleClick(object sender, EventArgs e)
        {
            LoginBtn.Enabled = false;
            MessageBox.Show("Testing this on djeklDevelopments \"Test Enviroment\"\n\nThis is a virgin IPB 3.1 with no modifications.\n\nYou should expect to see the following...\nUsername = \"ForumConnect\"\nUsergroup = \"Banned\"", "ForumConnect", MessageBoxButtons.OK, MessageBoxIcon.Information);
            var ForumLogin = new Login("https://www.djekldev.co.uk/ForumConnect_testing/IPB/ForumConnect.php", "eHl5cnpvZGR4ZWVyeW51cXNmeW5wcXZlZnpyZW5scmp3bGlobGZwbmdoa25nZ2Rvb256dGhwZmhkdGx6d3pwbTkyMzhYWVlSWk9ERFhFRVJZTlVRU0ZZTlBRVkVGWlJFTkxSSldMSUhMRlBOR0hLTkdHRE9PTlpUSFBGSERUTFpXWlBNTlZWTkZNRU1RS0dJR0lYWVZEUUFMWUJHRkJFU1RQUUhSWVpMTFFKQklNQ1dKWEFVVk5FVElQQVBUT0hESU1UQg==");
            MessageBox.Show(ForumLogin.PingServer() == false ? "Server Unavailable":"Server Available", "Ping Test");
            ForumLogin.Verify("ForumConnect", "ForumConnect");
            MessageBox.Show(ForumLogin.GetErrorCode == "true" ? String.Format("Username = {0}{1}Usergroup = {2}", ForumLogin.Username, Environment.NewLine, ForumLogin.Usergroup) : ForumLogin.GetErrorCode, ForumLogin.GetErrorCode == "true" ? "Login Shit" : "GetErrorCode");
            ForumLogin.unValidate();
            LoginBtn.Enabled = true;
        }

        private void FC_Info_DoubleClick(object sender, EventArgs e)
        {
            FC_URL.Text = null;
            SALT.Text = null;
        }

        private void LoginCreds_DoubleClick(object sender, EventArgs e)
        {
            UsernameTxt.Text = null;
            PasswordTxt.Text = null;
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            if (FC_URL.Text == string.Empty || SALT.Text == string.Empty || UsernameTxt.Text == string.Empty || PasswordTxt.Text == string.Empty || FC_URL.Text == null || SALT.Text == null || UsernameTxt.Text == null || PasswordTxt.Text == null)
            {
                MessageBox.Show("Not all information box's are filled in!", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Attempting login with information provided", "ForumConnect", MessageBoxButtons.OK, MessageBoxIcon.Information);
            var ForumLogin = new Login(FC_URL.Text, SALT.Text);
            MessageBox.Show(ForumLogin.PingServer() == false ? "Server Unavailable" : "Server Available", "Ping Test");
            ForumLogin.Verify(UsernameTxt.Text, PasswordTxt.Text);
            MessageBox.Show(ForumLogin.GetErrorCode == "true" ? String.Format("Username = {0}{1}Usergroup = {2}", ForumLogin.Username, Environment.NewLine, ForumLogin.Usergroup) : ForumLogin.GetErrorCode, ForumLogin.GetErrorCode == "true" ? "Login Shit" : "GetErrorCode");
            ForumLogin.unValidate();
        }
    }
}
