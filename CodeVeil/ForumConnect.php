<?PHP
/*
 * ForumConnect PHP Version 1.4
 * Strictly for usage with the ForumConnect.dll verseion 2.x
 * Coded by djeklDevelopments
 *
 * Special thanks to Oscar for starting this project and helping when I needed advice.
 *
 * Special thanks to v3n3 for helping me when learning C#.
 * Without your help m8 I would have been lost.
 */

$path = "./"; //Path to your forum relative to this PHP file, only change if this file is not in your forum folder.

$writeLog = true; // To write to the log or not
$LogFileName = "ForumConnect.log";

// Please change this!!
$SALT = "CHANGE ME";

/**************************************
******** Don't edit below this ********
**************************************/
/* No errors */
if ( !isset( $_REQUEST['debug'] ) )
	ini_set( 'display_errors', 0 );

$rc4a = base64_decode($SALT);

$user = $_GET['u'];
$pass = $_GET['p'];
$dynKey = $_GET['d'];

$dynKey = base64_decode(rc4(base64_decode($dynKey), $rc4a));
$user = rc4(base64_decode($user), $rc4a);
$pass = rc4(base64_decode($pass), $rc4a);

$strikeCount = 0;

if($_GET['act'] == "test"){
	$Output = rc4("ForumConnect by djekl Developments", $rc4a);
	echo base64_encode($Output);
	exit;
}

if($user == ""){
	die(rc4("No username given.", $dynKey));
}
elseif($pass == ""){
	die(rc4("No password given.", $dynKey));
}

$indexContents = file_get_contents($path."index.php")or die(rc4("Can't open 'index.php'", $dynKey));



# SMF 1.1 #
if(doesContain($indexContents, "$forum_version = 'SMF 1.1")){
	$forum = "smf1";
	verifySmf();
}

# SMF 2 #
elseif(doesContain($indexContents, "$forum_version = 'SMF 2.")){
	$forum = "smf2";
	verifySmf();
}

# MyBB 1 #
elseif(doesContain($indexContents, "MyBB 1.") && doesContain($indexContents, "Website: http://mybb.com")){
	$forum = "MyBB 1.6";
	verifyMyBB();
}

# vBulletin 3 #
elseif(doesContain($indexContents, "vBulletin 3") && doesContain($indexContents, "| http://www.vbulletin.com/license.html")){
	$forum = "vb3";
	verifyvB();
}

# vBulletin 4 #
elseif(doesContain($indexContents, "vBulletin 4") && doesContain($indexContents, "| http://www.vbulletin.com/license.html")){
	$forum = "vb4";
	verifyvB();
}

# IPB 2 #
elseif(doesContain($indexContents, "version	2.") && doesContain($indexContents, "@package	InvisionPowerBoard")){
	$forum = "ipb2";
	verifyIPB2();
}

# IPB 3 #
elseif(doesContain($indexContents, "IP.Board v3.") && (doesContain($indexContents, "@package		IP.Board") or doesContain($indexContents, "@package		Invision Power Board"))){
	$forum = "ipb3";
	verifyIPB3();
}

# phpBB 3 #
elseif(doesContain($indexContents, "@package phpBB3")){
	$forum = "phpbb3";
	verifyphpBB3();
}

# Unknown #
else{die(rc4("Forum type could not be determined.", $dynKey));}


### SMF = sha1(toLower(username).password) ###
function verifySmf(){
	global $user, $pass;
	
	require $path."Settings.php";
	$sha1Pass = sha1(strtolower($user).$pass);
	
	mysql_connect($db_server, $db_user, $db_passwd);
	mysql_select_db($db_name);
	
	/*				User Info				*/
	$result = mysql_query("SELECT * FROM `". $db_prefix . "members` WHERE `passwd` = '$sha1Pass'");
	
	if($result == false){
		prntOutput(false);
		exit;
	}
	
	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die(prntOutput(false));
		$userGroup = $row[id_group];
		$userName = $row[member_name];
	mysql_free_result($result);
	
	/*				Usergroup Info				*/
	$result = mysql_query("SELECT * FROM `". $db_prefix . "membergroups` WHERE `id_group` = '$userGroup'")or die(prntOutput(true, "SQL Error", "Fetch 2"));
		
	if($result == false){
		prntOutput(false);
		exit;
	}
	
	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die(prntOutput(false));
			$userGroupName = $row[group_name];
	
	if(mysql_num_rows($result) == 1){
		prntOutput(true, $userGroupName, $userName);
	}
	else{
		prntOutput(false);
	}

		mysql_free_result($result);

}

### vBulletin 3 & 4 = md5(md5(password).salt) ###
function verifyVb(){
	global $user, $pass;
	
	require $path."includes/config.php";
	
	mysql_connect($config['MasterServer']['servername'].":".$config['MasterServer']['port'], $config['MasterServer']['username'], $config['MasterServer']['password']);
	mysql_select_db($config['Database']['dbname']);
	
	$result = mysql_query("SELECT *  FROM `".$config['Database']['tableprefix']."user` WHERE `username` = '$user'");
	
	if($result == false){
		prntOutput(false);
		exit;
	}
	
	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die("False:0:False:0");
	
	$md5Pass = md5(md5($pass).$row["salt"]);
	
	$UserID = $row["userid"];	
		
	$GrpID = $row["usergroupid"];
	$groups = mysql_query("SELECT * FROM `".$config['Database']['tableprefix']."usergroup` WHERE `usergroupid` = '$GrpID'");
	
	$group_row = mysql_fetch_array($groups, MYSQL_ASSOC)or die("False:0:False:0");
	
	$userGroupName = $group_row["title"];
	
	if($row["password"] == $md5Pass){
		$userGroup = $userGroupName;
		$userName = $row["username"];
		prntOutput(true, $userGroup, $userName);
		}
        else{
		prntOutput(false);
	}
}

### IPB 2 md5(md5(salt).md5(password)) ###
function verifyIPB2(){
	global $user, $pass;
	
	require $path."conf_global.php";
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);
	
	$result = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."members` WHERE `name` = '$user'");
	
	if($result == false){
		prntOutput(false);
		exit;
	}
	
	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die("False:0:False:0");
	
	$userID = $row[id];
	$userName = $row[name];
	$userGroup = $row[mgroup];
	
	## djekl edit ##
	$groups = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."groups` WHERE `g_id` = '$userGroup'");
	
	$group_row = mysql_fetch_array($groups, MYSQL_ASSOC)or die("False:0:False:0");
	
	$userGroupName = $group_row[g_title];
	## djekl edit end ##
	
	$result = mysql_query("SELECT *  FROM `".$INFO['sql_tbl_prefix']."members_converge` WHERE `converge_id` = '$userID'");
	
	if($result == false){
		prntOutput(false);
		exit;
	}
	
	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die("False:0:False:0");

	
	$realHash = $row[converge_pass_hash];
	$salt = $row[converge_pass_salt];
	$md5Pass = md5(md5($salt).md5($pass));
	
	if($realHash == $md5Pass){
		#$userGroup = $userGroupName; //Un-Hash when we have the group name showing... ~djekl
		prntOutput(true, $userGroupName, $userName);
	}
	else{
		prntOutput(false);
	}
}

### IPB 3 md5(md5(salt).md5(password)) ###
function verifyIPB3(){
	global $user, $pass;
	
	require $path."conf_global.php";
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);
	
	$result = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."members` WHERE `name` = '$user'");
	
	if($result == false){
		prntOutput(false);
		exit;
	}

	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die("False:0:False:0");
	
	$UserID = $row[member_id];
	$userGroup = $row[member_group_id];
	$userName = $row[name];
	
	$groups = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."groups` WHERE `g_id` = '".addslashes($userGroup)."'");
	
	if($groups == false){
		prntOutput(false);
		exit;
	}

	$Usr_row = mysql_fetch_array($groups, MYSQL_ASSOC)or die("False:0:False:0");
	$userGroupName = $Usr_row[g_title];

	$realHash = $row[members_pass_hash];
	$salt = $row[members_pass_salt];
	$sha1Pass = md5(md5($salt).md5($pass));

	if($sha1Pass == $realHash){
		prntOutput(true, $userGroupName, $userName);
	}
	else{
		prntOutput(false);
		}
}

### MyBB 1.6 md5(md5($salt).md5($plain_pass)) ### MADE BY djekl ###
function verifyMyBB(){
	
	global $user, $pass;
	
	require $path."/inc/config.php";
	
	mysql_connect($config['database']['hostname'], $config['database']['username'], $config['database']['password']);
	mysql_select_db($config['database']['database']);
	
	$result = mysql_query("SELECT * FROM `".$config['database']['table_prefix']."users` WHERE `username` = '$user'");
	
	if($result == false){
		prntOutput(false);
		exit;
	}

	$row = mysql_fetch_array($result, MYSQL_ASSOC)or die("False:0:False:0");

	$UserID = $row[uid];
	$userGroup = $row[usergroup];
	$userName = $row[username];

	
	$groups = mysql_query("SELECT * FROM `".$config['database']['table_prefix']."usergroups` WHERE `gid` = '".$userGroup."'");
	
	if($groups == false){
		prntOutput(false);
		exit;
	}

	$Usr_row = mysql_fetch_array($groups, MYSQL_ASSOC)or die("False:0:False:0");
	
	$userGroupName = $Usr_row[title];
	$realHash = $row[password];
	$salt = $row[salt];
	$sha1Pass = md5(md5($salt).md5($pass));

	if($sha1Pass == $realHash){
		prntOutput(true, $userGroupName, $userName);
	}
	else{
		prntOutput(false);
		}
}

### phpBB3 = 'user_password' => phpbb_hash($data['new_password']) ### MADE BY djekl & Oscar ###
function verifyphpBB3(){
	global $user, $pass;
    
    require $path."config.php";
    
    mysql_connect($dbhost, $dbuser, $dbpasswd);
    mysql_select_db($dbname);
    
    $result = mysql_query("SELECT *  FROM `".$table_prefix."users` WHERE `username` = '$user'");
    
    if($result == false){
        prntOutput(false);
        exit;
    }
    
    $row = mysql_fetch_array($result, MYSQL_ASSOC) or die("False:0:False:0");
    
	$hash = $row[user_password];
	$userID = $row[user_id];
	$userGroup = $row[group_id];
    $userName = $row[username];
    
	$groups = mysql_query("SELECT *  FROM `".$table_prefix."users` WHERE `group_id` = '$userGroup'");
	
	if($groups == false){
		prntOutput(false);
		exit;
	}

	$Usr_row = mysql_fetch_array($groups, MYSQL_ASSOC)or die("False:0:False:0");
	
	$userGroupName = $Usr_row[group_name];

	if(phpbb_check_hash($pass, $hash)){
        prntOutput(true, $userGroupName, $userName);
	}
	else{
		prntOutput(false);
	}
}

function RemoveStrike(){ //Adds a login failed strike
global $user, $pass, $userName, $path, $ConfigFile;
	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$result = mysql_query("SELECT *  FROM `fc_trap` WHERE `ip` = '$ip'");
	
	if(mysql_num_rows($result) == 0){ //If the user has no strikes
		return;
	}

	$row = mysql_fetch_assoc($result);

	if ($row[strikeCount] > 0){
		$strike = $row[strikeCount] - 1;
		mysql_query("UPDATE `fc_trap` SET `strikeCount` = '$strike' WHERE `ip` = '$ip'");
	}
}

function addStrike(){ //Adds a login failed strike
global $user, $pass, $userName, $path, $ConfigFile, $strikeCount, $dynKey, $rc4a;
    require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$result = mysql_query("SELECT *  FROM `fc_trap` WHERE `ip` = '$ip'");
	
	
	if(mysql_num_rows($result) == 0){ //If the user has no strikes
		mysql_query("INSERT INTO `fc_trap` (`ip`, `strikeCount`, `timeOfStrike`) VALUES ('$ip', '1', CURRENT_TIMESTAMP);");
		$strikeCount = 1;
		return;
	}
	else
	{
	$row = mysql_fetch_assoc($result);

	$strike = $row[strikeCount] + 1;
	$strikeCount = $strike;

	mysql_query("UPDATE `fc_trap` SET `strikeCount` = '$strike' WHERE `ip` = '$ip'");
	}
}

function isBlocked(){ //Check if the current IP is blocked
 global $user, $pass, $userName, $path, $ConfigFile, $dynKey, $rc4a;
	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$result = mysql_query("SELECT *  FROM `fc_trap` WHERE `ip` = '$ip'");
	$row = mysql_fetch_row($result);
	
	if(mysql_num_rows($result) == 0){ //If the user has no strikes
		return false;
	}
	else{
		if((int)$row[1] > 10){ //If the lockout limit is over 10, the user is blocked.
			$token = "Blocked - password lockout";
			$token = rc4($token, $dynKey);
			echo $token;
			exit;
		}
	}
}

function prntOutput($isTrue, $userGroup = 0, $userName = 0){
	global $dynKey, $rc4a, $LogFileName;
	if($isTrue){
		$token = "true";
		$token .= "\n";
		$token .= $userGroup;
		$token .= "\n";
		$token .= $userName;
		$token .= "\n";
	
	}
	else{
		$token = "false";
	}
	
	$token = rc4($token, $dynKey);
	echo $token;
	
	## log FC usage ##
    if($writeLog){
		
		$log = fopen("./".$LogFileName, "a+");
		if($log){
			fwrite($log, $userName."|".$In."|".$IP."|".date("j/m/y - G:i:s")."\n");
			fclose($log);
          }
		else{
			$log = fopen($LogFileName, "a+");
			if($log){
				fwrite($log, $userName."|".$In."|".$IP."|".date("j/m/y - G:i:s")."\n");
				fclose($log);
			  }
		}
    }
	## log FC usage ##
}

function rc4 ($data, $pwd){
        $key[] = '';
        $box[] = '';
        $cipher = '';

        $pwd_length = strlen($pwd);
        $data_length = strlen($data);

        for ($i = 0; $i < 256; $i++)
        {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }
        for ($j = $i = 0; $i < 256; $i++)
        {
            $j = ($j + $box[$i] + $key[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for ($a = $j = $i = 0; $i < $data_length; $i++)
        {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ (0xff - $k));
        }
        return $cipher;
    }

function doesContain($haystack, $needle){
	if(strpos($haystack, $needle) == false){
		return false;
	}
	else{
		return true;
	}
}

function SQL_Logs_Table_Test($ConfigFile){
	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass'])or die(rc4("Couldn't connect!", $dynKey));
	mysql_select_db($INFO['sql_database']);

	$tbl_exists = mysql_query("DESCRIBE fc_trap");
	if (!$tbl_exists) {
		mysql_query("CREATE TABLE IF NOT EXISTS `fc_trap` (`ip` text NOT NULL,`strikeCount` text NOT NULL,`timeOfStrike` timestamp NOT NULL default CURRENT_TIMESTAMP) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
	}
}

### phpBB stuff ###
function phpbb_check_hash($password, $hash)
{
	$itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	if (strlen($hash) == 34)
	{
		return (_hash_crypt_private($password, $hash, $itoa64) === $hash) ? true : false;
	}

	return (md5($password) === $hash) ? true : false;
}
function _hash_encode64($input, $count, &$itoa64)
{
	$output = '';
	$i = 0;

	do
	{
		$value = ord($input[$i++]);
		$output .= $itoa64[$value & 0x3f];

		if ($i < $count)
		{
			$value |= ord($input[$i]) << 8;
		}

		$output .= $itoa64[($value >> 6) & 0x3f];

		if ($i++ >= $count)
		{
			break;
		}

		if ($i < $count)
		{
			$value |= ord($input[$i]) << 16;
		}

		$output .= $itoa64[($value >> 12) & 0x3f];

		if ($i++ >= $count)
		{
			break;
		}

		$output .= $itoa64[($value >> 18) & 0x3f];
	}
	while ($i < $count);

	return $output;
}
function _hash_crypt_private($password, $setting, &$itoa64)
{
	$output = '*';

	// Check for correct hash
	if (substr($setting, 0, 3) != '$H$')
	{
		return $output;
	}

	$count_log2 = strpos($itoa64, $setting[3]);

	if ($count_log2 < 7 || $count_log2 > 30)
	{
		return $output;
	}

	$count = 1 << $count_log2;
	$salt = substr($setting, 4, 8);

	if (strlen($salt) != 8)
	{
		return $output;
	}
	if (PHP_VERSION >= 5)
	{
		$hash = md5($salt . $password, true);
		do
		{
			$hash = md5($hash . $password, true);
		}
		while (--$count);
	}
	else
	{
		$hash = pack('H*', md5($salt . $password));
		do
		{
			$hash = pack('H*', md5($hash . $password));
		}
		while (--$count);
	}

	$output = substr($setting, 0, 12);
	$output .= _hash_encode64($hash, 16, $itoa64);

	return $output;
}
### phpBB stuff end ###


################################################################
#       ::::    :::  :::::::: ::::::::::: :::::::::: ::::::::  #
#      :+:+:   :+: :+:    :+:    :+:     :+:       :+:    :+:  #
#     :+:+:+  +:+ +:+    +:+    +:+     +:+       +:+          #
#    +#+ +:+ +#+ +#+    +:+    +#+     +#++:++#  +#++:++#++    #
#   +#+  +#+#+# +#+    +#+    +#+     +#+              +#+     #
#  #+#   #+#+# #+#    #+#    #+#     #+#       #+#    #+#      #
# ###    ####  ########     ###     ########## ########        #
################################################################
/*
 * ForumConnect PHP Version 1.0
 * Strictly for usage with the ForumConnect.dll verseion 2.x
 * Coded by djeklDevelopments
 *
 * Special thanks to Oscar for starting this project and helping when I needed advice.
 *
 * Special thanks to v3n3 for helping me when learning C#.
 * Without your help m8 I would have been lost.
 */
?>