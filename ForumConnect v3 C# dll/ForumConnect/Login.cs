﻿namespace ForumConnect
{
    using System;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;

    public class Login
    {
        private bool _debug = false;
        private bool isverified;
        private string usergroup;
        private string usern;
        private readonly string urlz;
        private readonly string Passw0rdz;
        private string ErrLine;
        private byte[] dynKey;

        /// <summary>
        /// This is used to connect the dll to the url.
        /// </summary>
        /// <param name="URL">Your URL To the ForumConnect PHP Script</param>
        /// <param name="Private_Key">Your PRIVATE ForumConnect Salt</param>
        public Login(string URL, string Private_Key)
        {
            urlz = URL;
            Passw0rdz = Private_Key;
            CheckArguments(urlz, Passw0rdz);
        }

        private static byte[] genKey()
        {
            return new DESCryptoServiceProvider().Key;
        }

        private byte[] PrivateSaltKey(byte[] dnyKey)
        {
            byte[] rc4a = Convert.FromBase64String(Passw0rdz);
            int newSize = dnyKey.Length + rc4a.Length;
            var ms = new MemoryStream(new byte[newSize], 0, newSize, true, true);
            ms.Write(dnyKey, 0, dnyKey.Length);
            ms.Write(rc4a, 0, rc4a.Length);
            return ms.GetBuffer();
        }

        private static byte[] rc4(byte[] bytes, byte[] key)
        {
            byte[] s = new byte[256];
            byte[] k = new byte[256];
            byte temp;

            int i = 0;
            for (i = 0; i < 256; i++)
            {
                s[i] = (byte)i;
                k[i] = key[i % key.GetLength(0)];
            }

            int j = 0;
            for (i = 0; i < 256; i++)
            {
                j = (j + s[i] + k[i]) % 256;
                temp = s[i];
                s[i] = s[j];
                s[j] = temp;
            }

            i = j = 0;
            for (int x = 0; x < bytes.GetLength(0); x++)
            {
                i = (i + 1) % 256;
                j = (j + s[i]) % 256;
                temp = s[i];
                s[i] = s[j];
                s[j] = temp;
                int t = (s[i] + s[j]) % 256;
                bytes[x] ^= (byte)(0xff - s[t]);
            }
            return bytes;
        }

        /// <summary>
        /// This simply checks for a valid responce from the PHP script.
        /// </summary>
        /// <returns>True/False</returns>
        public bool PingServer()
        {
            try
            {
                byte[] rc4a = Convert.FromBase64String(Passw0rdz);
                string url = urlz + "?act=test";
                var reader = new StreamReader(new MemoryStream(new WebClient().DownloadData(url)));
                String responce = reader.ReadToEnd();

                if (_debug)
                {
                    // Debug only. Output the responce.
                    string __debugResponce = String.Format("{0}\n{1}\n{2}", responce, Convert.ToBase64String(rc4(Encoding.UTF8.GetBytes("ForumConnect 3.x by djekl Developments"), rc4a)), "ForumConnect 3.x by djekl Developments");
                    System.Windows.Forms.MessageBox.Show(__debugResponce, "Debugging", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                }
                
                if (responce == Convert.ToBase64String(rc4(Encoding.ASCII.GetBytes("ForumConnect 3.x by djekl Developments"), rc4a)))
                {
                    return true;
                }
                return false;
           }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This is used to "Logout" the user from your application.
        /// Please use it!
        /// </summary>
        public void unValidate()
        {
            isverified = false;
            usern = null;
            usergroup = null;
        }

        /// <summary>
        /// This is used to verify that the user has entered the correct username and password.
        /// </summary>
        /// <param name="user">Username</param>
        /// <param name="pass">Password</param>
        /// <returns>True/False</returns>
        public bool Verify(string user, string pass)
        {
            try
            {
                byte[] rc4a = Convert.FromBase64String(Passw0rdz);
                dynKey = genKey();
                string userEnc = HttpUtility.UrlEncode(Convert.ToBase64String(rc4(Encoding.ASCII.GetBytes(user), rc4a)));
                string passEnc = HttpUtility.UrlEncode(Convert.ToBase64String(rc4(Encoding.ASCII.GetBytes(pass), rc4a)));
                string key = HttpUtility.UrlEncode(Convert.ToBase64String(rc4(Encoding.ASCII.GetBytes(Convert.ToBase64String(dynKey)), rc4a)));

                string url = String.Format("{0}?u={1}&p={2}&d={3}", urlz, userEnc, passEnc, key);

                var reader = new StreamReader(new MemoryStream(rc4(new WebClient().DownloadData(url), dynKey)));

                // This is the error line to be used later...
                ErrLine = reader.ReadLine();

                if (ErrLine != "true")
                {
                    unValidate();
                    return false;
                }

                usergroup = reader.ReadLine();
                usern = reader.ReadLine();

                if (_debug)
                {
                    System.Windows.Forms.MessageBox.Show(ErrLine + Environment.NewLine + usern + Environment.NewLine + usergroup, "Debugging", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                }

                isverified = true;

                return true;
            }
            catch
            {
                unValidate();
                return false;
            }
        }

        /// <summary>
        /// Checks if the user has been verified by the script/dll.
        /// </summary>
        public bool isValidated
        {
            get
            {
                return isverified;
            }
        }

        /// <summary>
        /// This returns the currently logged in usergroup.
        /// If your not validated by the dll then it returns "Not currently logged in!"
        /// </summary>
        public string Usergroup
        {
            get
            {
                return isValidated ? usergroup : "Not currently logged in!";
            }
        }

        /// <summary>
        /// This returns the currently logged in username (as formatted by the user).
        /// If your not validated by the dll then it returns "Not currently logged in!"
        /// </summary>
        public string Username
        {
            get
            {
                return isValidated ? usern : "Not currently logged in!";
            }
        }

        /// <summary>
        /// This is used for the retreval of the Error Code's.
        /// </summary>
        public string GetErrorCode
        {
            get
            {
                return ErrLine;
            }
        }

        private void CheckArguments(String urlz = "N/A", String Passw0rdz = "N/A")
        {
            string[] xArgs = Environment.GetCommandLineArgs();
            Array.ForEach(xArgs, str =>
            {
                if (str.ToLower() == "-debug")
                {
                    _debug = true;
                    string xPath = Environment.CurrentDirectory + @"\[debug] ForumConnect.txt";
                    var sb = new StringBuilder();
                    sb.AppendLine("This Application is using ForumConnect created by djekl Developments");
                    sb.AppendLine(String.Format("DLL Version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version));
                    sb.AppendLine();
                    //sb.AppendLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                    //sb.AppendLine();
                    //sb.AppendLine("The following information is YOUR PRIVATE INFO!");
                    //sb.AppendLine(String.Format("URL: {0}", urlz));
                    //sb.AppendLine(String.Format("SALT: {0}", Passw0rdz));
                    //sb.AppendLine();
                    //sb.AppendLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                    //sb.AppendLine();
                    File.Create(xPath).Close();
                    File.AppendAllText(xPath, sb.ToString());
                }
                if (str.ToLower() == "-djekldevelopments")
                {
                    System.Windows.Forms.MessageBox.Show(String.Format("ForumConnect by djekl Developments!\n\nForumConnect v{0}\n\nSpecial thanks to v3n3 for helping me when learning C#.\nWithout your help m8 I would have been lost.", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version), "Credit", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                    string xPath = Environment.CurrentDirectory + "\\ForumConnect.txt";
                    var sb = new StringBuilder();
                    sb.AppendLine("This Application is using ForumConnect created by djekl Developments");
                    sb.AppendLine(String.Format("DLL Version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version));
                    sb.AppendLine();
                    sb.AppendLine("Really, is it so fucking hard to give credit?");
                    sb.AppendLine();
                    sb.AppendLine("Special thanks to v3n3 for helping me when learning C#.");
                    sb.AppendLine("Without your help m8 I would have been lost.");
                    sb.AppendLine();
                    sb.AppendLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                    sb.AppendLine();
                    sb.AppendLine("The following information is YOUR PRIVATE INFO!");
                    sb.AppendLine(String.Format("URL: {0}", urlz));
                    sb.AppendLine(String.Format("SALT: {0}", Passw0rdz));
                    sb.AppendLine();
                    sb.AppendLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                    sb.AppendLine();
                    sb.AppendLine("Learn to give credit!");
                    File.Create(xPath).Close();
                    File.AppendAllText(xPath, sb.ToString());
                    System.Threading.Thread.CurrentThread.Abort("Give Credit CUNT!");
                    throw new Exception("ForumConnect by djekl Developments!");
                }
            });
        }
    }

}

