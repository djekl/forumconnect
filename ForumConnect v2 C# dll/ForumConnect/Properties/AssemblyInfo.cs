﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ForumConnect v2.1.1.0")]
[assembly: AssemblyDescription("This is used to link your Forum users to your application.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("djekl Developments")]
[assembly: AssemblyProduct("ForumConnect")]
[assembly: AssemblyCopyright("Copyright © djekl Developments 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("11e6f14b-e562-485e-b235-d549f3fd9794")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.1.1.4")]
[assembly: AssemblyFileVersion("2.1.1.4")]
