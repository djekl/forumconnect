<?PHP
/*
 * ForumConnect PHP Version 1.0
 * Strictly for usage with the ForumConnect.dll verseion 2.x
 * Coded by djeklDevelopments
 *
 * Special thanks to Oscar for starting this project and helping when I needed advice.
 *
 * Special thanks to v3n3 for helping me when learning C#.
 * Without your help m8 I would have been lost.
 */

$path = "./"; //Path to your forum relative to this PHP file, only change if this file is not in your forum folder.

$writeLog = true; // To write to the log or not
$LogFileName = "ForumConnect.log";

// Please change this!!
$SALT = "dXR2Ynp1ZHFiZnB4bWthd3loaXh4eW5scXdhdWdvemdueWtpcmJ1YnhvZ2Z0Y3p5d2t0bGp0bHZma2tjdWlqbjc5NzdVVFZCWlVEUUJGUFhNS0FXWUhJWFhZTkxRV0FVR09aR05ZS0lSQlVCWE9HRlRDWllXS1RMSlRMVkZLS0NVSUpORkpKT1pTRlhNSEFKU0lST1hCWFdVTlhVTU9DR1FSSE9FU1pLU0tZRUVXWkJUQ0RYU1dGU0tOU1NNUENLUFlHVQ==";

/**************************************
******** Don't edit below this ********
**************************************/
/* No errors */
if ( !isset( $_REQUEST['debug'] ) )
	ini_set( 'display_errors', 0 );

$rc4a = base64_decode($SALT);

$user = $_GET['u'];
$pass = $_GET['p'];
$dynKey = $_GET['d'];

$dynKey = base64_decode($dynKey);
$user = rc4(base64_decode($user), $rc4a);
$pass = rc4(base64_decode($pass), $rc4a);

$strikeCount = 0;

if($_GET['act'] == "test"){
	echo rc4("ForumConnect by djekl Developments", $rc4a);
	exit;
}

if($user == ""){
	die(rc4("No username given.", $dynKey));
}
elseif($pass == ""){
	die(rc4("No password given.", $dynKey));
}

$indexContents = file_get_contents($path."index.php")or die(rc4("Can't open 'index.php'", $dynKey));

if(doesContain($indexContents, "IP.Board v3.") && (doesContain($indexContents, "@package		IP.Board") or doesContain($indexContents, "@package		Invision Power Board"))){
	$forum = "ipb3";
	verifyIPB3();
}
else{
	die(rc4("Forum type could not be determined.", $dynKey));
}

### IPB 3 md5(md5(salt).md5(password)) ###
function verifyIPB3(){
global $user, $pass, $userName, $path, $ConfigFile, $strikeCount, $dynKey, $rc4a;
	
	$ConfigFile = $path."conf_global.php";

	// Ban table test!
	SQL_Logs_Table_Test($ConfigFile);
	
	// Is this IP Banned?
	isBlocked();

	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);
	
	$result = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."members` WHERE `name` = '$user'");
	
	if($result == false){
		prntOutput(false);
		echo "Username Not Found.";
		exit;
	}

	$row = mysql_fetch_row($result);
	
	$UserID = $row[0];
	$userGroup = $row[2];
	$userName = $row[51];
	
	if(strtolower($user) != strtolower($userName))
	{
		die(rc4("Username Not Found, you have ".addStrike()."".(10 - $strikeCount)." of a maximum 10 strikes remaining. You will then be banned from this system.", $dynKey));
	}
	
	$groups = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."groups` WHERE `g_id` = '".addslashes($userGroup)."'");
	$profile_portal = mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."profile_portal` WHERE `pp_member_id` = '".addslashes($UserID)."'");	
	
	$Usr_row = mysql_fetch_row($groups);
	$pp_row = mysql_fetch_row($profile_portal);
	
	$userGroupName = $Usr_row[20];
	$Avatar = $pp_row[30];

	$realHash = $row[61];
	$salt = $row[62];
	$sha1Pass = md5(md5($salt).md5($pass));

	if($sha1Pass == $realHash){
        RemoveStrike();
		$userGroup = $userGroupName;
		prntOutput(true, $userGroup, $userName);
	}else{
		#prntOutput(false);
		die(rc4("Bad Password, you have ".addStrike()."".(10 - $strikeCount)." of a maximum 10 strikes remaining. You will then be banned from this system.", $dynKey));
	}
}

function RemoveStrike(){ //Adds a login failed strike
global $user, $pass, $userName, $path, $ConfigFile;
	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$result = mysql_query("SELECT *  FROM `fc_trap` WHERE `ip` = '$ip'");
	
	if(mysql_num_rows($result) == 0){ //If the user has no strikes
		return;
	}

	$row = mysql_fetch_assoc($result);

	if ($row[strikeCount] > 0){
		$strike = $row[strikeCount] - 1;
		mysql_query("UPDATE `fc_trap` SET `strikeCount` = '$strike' WHERE `ip` = '$ip'");
	}
}

function addStrike(){ //Adds a login failed strike
global $user, $pass, $userName, $path, $ConfigFile, $strikeCount, $dynKey, $rc4a;
    require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$result = mysql_query("SELECT *  FROM `fc_trap` WHERE `ip` = '$ip'");
	
	
	if(mysql_num_rows($result) == 0){ //If the user has no strikes
		mysql_query("INSERT INTO `fc_trap` (`ip`, `strikeCount`, `timeOfStrike`) VALUES ('$ip', '1', CURRENT_TIMESTAMP);");
		$strikeCount = 1;
		return;
	}
	else
	{
	$row = mysql_fetch_assoc($result);

	$strike = $row[strikeCount] + 1;
	$strikeCount = $strike;

	mysql_query("UPDATE `fc_trap` SET `strikeCount` = '$strike' WHERE `ip` = '$ip'");
	}
}

function isBlocked(){ //Check if the current IP is blocked
 global $user, $pass, $userName, $path, $ConfigFile, $dynKey, $rc4a;
	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	mysql_select_db($INFO['sql_database']);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$result = mysql_query("SELECT *  FROM `fc_trap` WHERE `ip` = '$ip'");
	$row = mysql_fetch_row($result);
	
	if(mysql_num_rows($result) == 0){ //If the user has no strikes
		return false;
	}
	else{
		if((int)$row[1] > 10){ //If the lockout limit is over 10, the user is blocked.
			$token = "Blocked - password lockout";
			$token = rc4($token, $dynKey);
			echo $token;
			exit;
		}
	}
}

function prntOutput($isTrue, $userGroup = 0, $userName = 0){
	global $dynKey, $rc4a, $LogFileName;
	if($isTrue){
		$token = "true";
		$token .= "\n";
		$token .= $userGroup;
		$token .= "\n";
		$token .= $userName;
		$token .= "\n";
	
	}
	else{
		$token = "false";
	}
	
	$token = rc4($token, $dynKey);
	echo $token;
	
	## log FC usage ##
    if($writeLog){
			$log = fopen("./".$LogFileName, "a+");
		if($log){
			fwrite($log, $userName."|".$In."|".$IP."|".date("j/m/y - G:i:s")."\n");
			fclose($log);
          }	
    }
	## log FC usage ##
}

function rc4 ($data, $pwd){
        $key[] = '';
        $box[] = '';
        $cipher = '';

        $pwd_length = strlen($pwd);
        $data_length = strlen($data);

        for ($i = 0; $i < 256; $i++)
        {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }
        for ($j = $i = 0; $i < 256; $i++)
        {
            $j = ($j + $box[$i] + $key[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for ($a = $j = $i = 0; $i < $data_length; $i++)
        {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ (0xff - $k));
        }
        return $cipher;
    }

function doesContain($haystack, $needle){
	if(strpos($haystack, $needle) == false){
		return false;
	}
	else{
		return true;
	}
}

function SQL_Logs_Table_Test($ConfigFile){
	require $ConfigFile;
	
	mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass'])or die(rc4("Couldn't connect!", $dynKey));
	mysql_select_db($INFO['sql_database']);

	$tbl_exists = mysql_query("DESCRIBE fc_trap");
	if (!$tbl_exists) {
		mysql_query("CREATE TABLE IF NOT EXISTS `fc_trap` (`ip` text NOT NULL,`strikeCount` text NOT NULL,`timeOfStrike` timestamp NOT NULL default CURRENT_TIMESTAMP) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
	}
}

################################################################
#       ::::    :::  :::::::: ::::::::::: :::::::::: ::::::::  #
#      :+:+:   :+: :+:    :+:    :+:     :+:       :+:    :+:  #
#     :+:+:+  +:+ +:+    +:+    +:+     +:+       +:+          #
#    +#+ +:+ +#+ +#+    +:+    +#+     +#++:++#  +#++:++#++    #
#   +#+  +#+#+# +#+    +#+    +#+     +#+              +#+     #
#  #+#   #+#+# #+#    #+#    #+#     #+#       #+#    #+#      #
# ###    ####  ########     ###     ########## ########        #
################################################################
/*
 * ForumConnect PHP Version 1.0
 * Strictly for usage with the ForumConnect.dll verseion 2.x
 * Coded by djeklDevelopments
 *
 * Special thanks to Oscar for starting this project and helping when I needed advice.
 *
 * Special thanks to v3n3 for helping me when learning C#.
 * Without your help m8 I would have been lost.
 */
?>